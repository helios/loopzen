(ns loopzen.core
  (:require [reagent.core :as r]
            [loopzen.board :as b]
            [franz.core :as f]))

(enable-console-print!)

(defonce app-state (r/atom {:current-page :home
                            :game-board nil}))
(defonce main-topic (f/topic (f/log)))

#_[:iframe {:width "100px"
            :height "100px"
            :scrolling "no"
            :frameborder "no"
            :src "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/58173187&amp;
            auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;
            show_user=true&amp;show_reposts=false&amp;visual=true"}]

(defn render-board [{:keys [size cells]} update-fn]
  [:div
   [:div.table
    (doall
      (map-indexed
        (fn [i row]
          [:div {:class "row"
                 :key i}
           (doall
             (map-indexed
               (fn [j c]
                 (let [index (+ (* i size) j)]
                   [:img {:key (str i j)
                          :class (str "cell rotate-" (get-in cells [index :status]))
                          :src (str "/tiles/" (name (:tile c)) ".svg")
                          :width (str (Math/floor (/ 100 size)) "%")
                          :height (str (Math/floor (/ 100 size)) "%")
                          :on-click #(update-fn index)}]))
               row))])
        (partition size cells)))]])


(defmulti handle-message
          (fn [_state msg] (first msg)))

(defmethod handle-message :rotate-tile
  [state [_cmd index]]
  (update-in state [:game-board :cells index :status] #(rem (inc %) 4)))

(defn init-board []
  (->> b/board-pattern
       (b/create-board)
       (b/scramble)))

(defmethod handle-message :start-game
  [state [_cmd]]
  (-> state
      (assoc :game-board (init-board))
      (assoc :current-page :play)))

(defmethod handle-message :end-game
  [state [_cmd]]
  (assoc state :current-page :home))

(defmethod handle-message :reset-game
  [state [_cmd]]
  (assoc state :game-board (init-board)))

(defn init-handler []
  (f/subscribe! main-topic
                :main
                (f/sparse-handler
                  (fn [_o [_key msg]]
                    (swap! app-state handle-message msg)))))

(defn home-button []
  [:button.btn {:on-click #(f/send! main-topic [:end-game])}
   "Home"])

(defn reset-button []
  [:button.btn {:on-click #(f/send! main-topic [:reset-game])}
   "Reset"])

(defn play-button []
  [:button.btn {:on-click #(f/send! main-topic [:start-game])} "Play"])

(defn play-component []
  [:div
   [home-button]
   [reset-button]
   [:audio {:src "https://ia800801.us.archive.org/17/items/jamendo-099778/09.mp3" :controls true}]
   (render-board (:game-board @app-state)
                 #(f/send! main-topic [:rotate-tile %]))])

(defn home-component []
  [:div
   [play-button]])

(defn header []
  [:h2.title "Harmony Loops"])

(defn base-component []
  (init-handler)
  (fn []
    [:div
     [header]
     (case (:current-page @app-state)
       :home [home-component]
       :play [play-component])]))

(r/render-component [base-component]
                    (. js/document (getElementById "app")))

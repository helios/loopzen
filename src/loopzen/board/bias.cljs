(ns loopzen.board.bias
  (:require [reagent.core :as reagent :refer [atom]]
            [loopzen.board :as b]))

(def fill-rate 0.4)
(def bias-rate 0.02)

(defn create-grid [n]
  {:grid (into [] (repeat (* n n) 0))
   :size n})


(defn probability-distribution [g]
  (map-indexed (fn [i p]
                 (cond
                   (= p 1) 0
                   (not (empty? (b/neighbors #(= % 1) g i))) (min 1 (+ bias-rate (rand)))
                   :else (rand)))
               (:grid g)))


(defn filling [{:keys [grid]}]
  (count (filter #(= 1 %)
                 grid)))


(defn initialize-board [n threshold]
  (loop [board (create-grid n)]
    (if (>= (/ (filling board)
               (* (:size board)
                  (:size board)))
            threshold)
      board
      (let [pd (probability-distribution board)
            chosen-element (.indexOf (to-array pd) (apply max pd))
            new-board (assoc-in board [:grid chosen-element] 1)]
        (recur new-board)))))

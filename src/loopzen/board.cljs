(ns loopzen.board)

(def board-pattern
  {:size 4
   :pattern [0 0 0 1
             1 1 0 1
             0 0 1 1
             1 1 0 0]})

(defn tile [n-set]
  (case (count n-set)
    4 :4-tile
    3 :3-tile
    2 (if (#{#{:N :S} #{:E :W}} n-set)
        :2-tile-I
        :2-tile-L)
    1 :1-tile
    :0-tile))

(defn create-cell [value n-set tile]
  {:value value
   :neighbors n-set
   :tile tile
   :status 0})

(defn neighbors [f {:keys [size pattern]} index]
  (let [north? (f (get pattern (- index size)))
        south? (f (get pattern (+ index size)))
        west? (and (= (quot index size)
                      (quot (- index 1) size))
                   (f (get pattern (- index 1))))
        east? (and (= (quot index size)
                      (quot (+ index 1) size))
                   (f (get pattern (+ index 1))))]
    (->> [(when north? :N)
          (when west? :W)
          (when east? :E)
          (when south? :S)]
         (remove nil?)
         (into #{}))))

(defn board->neighbor [b]
  (map-indexed (fn [index p]
                 (if (= 1 p)
                   (neighbors #(= % 1) b index)
                   #{}))
               (:pattern b)))


(defn create-board [{:keys [size] :as bp}]
  (let [neighbors (board->neighbor bp)
        tiles (map tile neighbors)]
    {:size size
     :cells (mapv create-cell (:pattern bp) neighbors tiles)}))


(defn scramble [board]
  (update-in board
             [:cells]
             (fn [cells]
               (mapv #(assoc % :status (rand-int 4))
                     cells))))

(def correct-positions
  {#{:N :S :W :E} #{0 1 2 3}
   #{:W :N :E} #{0}
   #{:N :E :S} #{1}
   #{:E :S :W} #{2}
   #{:S :W :N} #{3}
   #{:N :S} #{0 2}
   #{:W :E} #{1 3}
   #{:N :E} #{0}
   #{:E :S} #{1}
   #{:S :W} #{2}
   #{:W :N} #{3}
   #{:N} #{0}
   #{:E} #{1}
   #{:S} #{2}
   #{:W} #{3}
   #{} #{0 1 2 3}})                                         ;


(defn correct-position? [cell]
  (contains?
   (get correct-positions (:neighbors cell))
   (:status cell)))

# loopzen

Clojurescript remake of "Loops of Zen"

## Running

    lein figwheel
    
Is sufficient to get the application up and running. Note that there is middleware support for CLJS repl (working fine in Emacs, at least).

To create a production build run:

    lein do clean, cljsbuild once min

And open your browser in `resources/public/index.html`. You will not
get live reloading, nor a REPL. 
